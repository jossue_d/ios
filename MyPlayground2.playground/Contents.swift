//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"   //variable
let str2 = "Heelo, playground 2" //constante

str = "something else"
//str2 = "something" -> Error defined as let

let 🍐 = 1 //emoji control command space

//str = 3 -> Error int to string

let myBol = true
let myBol2 = false

let myInt = 5
let myDouble = 3.5

print("Hello")
print(str)
print(myBol)
print(myDouble)

print("My int is: \(myInt)")
print(🍐)

print (Double(myInt) + myDouble)
let 🍎 = 4
print(2*🍎+🍐)

//Tuples
let coordinates = (5, 2)
let coordinatesDouble = (5.1, 5.3)
let coordinatesMixed = (5, 2.2)

let coordinates3D = (1,2,3)

let (x, y, z) = (0,9,8)

x
y
z

// || or && and >= <= == !=

//Flow controls
if x < 5 {
    print("It's great!")
}

//while
var counter = 0

while counter < 10 {
    counter
    counter += 1
}

repeat{
    counter -= 1
} while counter > 0

//for
for _ in 1...5 {
    print("ayy")
}

for i in 1...5{
    print(i)
}

for i in 1...20 where i % 2==0 {
    print (i)
}

let array = [1,2,3,4,5,6,7,8,9,10]

for n in array where n % 2==0 {
    print(n)
}

//switch
let age = 5

switch age {
case 0...17 where age % 2  == 0:
    print("Menor de edad")
case 18...64:
    print("adulto")
default:
    print("xd")
}

//FUNCIONES
//retorna void
func myFunction(){
    print("Hello")
}

myFunction()

//privada por clase
private func myPrivateFunction(){
    print("Hello from private")
}

//se hace privada solo por archivo y no por clase
fileprivate func myFilePrivateFunction(){
    print("Hello from file private")
}

func intFunction() -> Int {
    return 5
}

intFunction()

func calcFunction() -> (Int, Int){
    return (5+5, 5*5)
}

calcFunction()

func calcFunction2() -> (sum:Int, mult:Int){
    return (5+5, 5*5)
}

calcFunction2()
calcFunction2().mult
calcFunction2().sum


func calcFunction21() -> (sum:Int, mult:Int, Bool){
    return (5+5, 5*5, false)
}

func calcFunction3(n:Int) -> (sum:Int, mult:Int, Bool){
    return (5+5, 5*5, false)
}
calcFunction3(n:7)

func addOne (number n:Int) -> Int {
    return n+1
}

addOne(number: 9)

func add(_ n1:Int, _ n2:Int) -> Int {
    return n1 + n2
}

add(1, 2)

func addFour(_ n1:Int,_ n2:Int,_ n3:Int = 0,_ n4:Int = 0) -> Int { //para no especificar la variable
    return n1 + n2 + n3 + n4
}

addFour(1, 2)
addFour(1, 2, 3, 4)
addFour(1, 2, 3)

//Optionals
let myOptionalInt = Int("9")

print(myOptionalInt)

print(myOptionalInt!) //no usar porque forza quitar la opcionalidad

if let internalInt = myOptionalInt {
    print(internalInt)
}

print(myOptionalInt ?? 0) //reemplazar opcional con 0

func unwrapOptional(n:Int?){
    guard let myInt = n else { //guarda la variable en n
        return
    }
    print(myInt)
}

unwrapOptional(n: nil)

unwrapOptional(n: 4)

var myVar:Double = 50

myVar = 50.5

//Collections
var evenNumbers = [2, 4, 6, 8]
let evenNumbers2: Array<Int> = [2, 4, 6, 8]
let evenNumbers3:[Int] = [10, 12, 14]
let myArray:[Double] = []

evenNumbers.append(18)

evenNumbers += [14]

evenNumbers.isEmpty
evenNumbers.count
evenNumbers.first

if let first = evenNumbers.first {
    print(first)
}
    else{
      print("Nothing")
    }

evenNumbers.max()
evenNumbers.min()

evenNumbers.contains(4)

// print(myArray[0]) //indice fuera de rango error porque esta vacio

let customArray = evenNumbers[0...2]
evenNumbers.insert(90, at: 0)

//Dictionary
var myScores = ["Sebas": 10, "Andres": 12]

print(myScores["Sebas"])

print(myScores["Cristian"] ?? 0)

myScores["Cristian"] = 80

for (player, score) in myScores {
    print("Player \(player) score is \(score)")
}

//Sets

var someSet:Set <Int> = [2,1,1,23,4] //no importa el orden

someSet.contains(1)
someSet.isEmpty
someSet.insert(28)

//CLASS 4 de Darwin
//CLOSURES
var multiplyClosure: (Int, Int) -> (Int)

multiplyClosure = { (a:Int, b:Int) in
    return a * b
}
let result = multiplyClosure(3, 6)

//la misma de arriba
var multiplyClosure2: (Int, Int) -> Int = {$0 * $1}
let result2 = multiplyClosure2(5, 4)

//closure como parametro
func operateNumbers(
    _ a:Int,
    _ b:Int,
    operation:(Int, Int) -> (Int)
    ) -> Int {
    return operation(a,b)
}

operateNumbers(5, 3, operation: multiplyClosure)

operateNumbers(10, 2, operation: {$0/$1})


//SORT
var numbers = [1, 3, 5, 9, 5, 12, 10, 29, 39, 48, 27, 50]

//sort asc
numbers.sorted()

//sort desc
numbers.sort { (a, b) -> Bool in
    return a > b
}

//sort desc
//para usar este se debe tener arreglos unicamente tipo var
numbers.sort{$0 > $1}

//sort desc rapido
numbers.sorted().reversed()

//PROGRAMACION FUNCIONAL
let prices = [10.4, 11, 5.5, 2, 3, 4.8, 20]

//FILTER
//prog. imperativa
let filtered = prices.filter { (price) -> Bool in
    return price > 8
}
//prog. funcional
let filtered2 = prices.filter{$0 > 8}
filtered2

//MAP
let onSalePrices = prices.map{$0 * 0.9}
onSalePrices

//REDUCE
let sum = prices.reduce(0, {$0 + $1})
sum



//STRINGS
let name = "Sebas"
let message = "Hello, " + name
//para escribir strings en varias lineas """
let message2 = """
Super
super
super long
string
"""

for char in name {
    print(char)
}

name.count

let cafeNormal = "café"

//strings son UNICODE
let cafeComb = "cafe\u{0301}"
print(cafeComb)

//tienen el mismo numero de caracteres (caracteristica de swift que ahorra memoria)
cafeNormal.count
cafeComb.count


//STRUCTS VS CLASES
//struc hace paso por valor y las clases por referencia

//STRUCTS
struct Person{
    let firstName:String
    let lastName: String
    //compute property siempre var
    var fullName:String {
        get {
            return firstName + " " + lastName
        }
    }
    //corre solo cuando se cambia la variable
    var age:Int {
        didSet {
            if age < oldValue {
                age = oldValue
            }
            print("Age changed from \(oldValue) to \(age)")
        }
    }
    func printName(){
        print("My name is \(name)")
    }
}

let p1 = Person(firstName: "Sebas", lastName: "Guzmán", age: 26)
//se pasa por valor, es decir q son independientes
var p2 = p1

p1.fullName
p2.age = 30
//no deja por didset
p2.age = 25

p1.age
p2.age

p1.printName()


//CLASS
class PersonClass{
    let firstName:String
    let lastName:String
    var age:Int
    
    var fullName:String{
        return firstName + " " + lastName
    }
    
    init(firstName:String, lastName:String, age:Int){
        self.firstName = firstName
        self.lastName = lastName
        self.age = age
    }
}

let p3 = PersonClass(firstName: "Sebas2", lastName:"Guzman2", age:26)
let p4 = p3
p3.age
p4.age
p4.age = 50
p3.age
p4.age

