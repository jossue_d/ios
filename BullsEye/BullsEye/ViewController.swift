//
//  ViewController.swift
//  BullsEye
//
//  Created by Jossué Dután on 24/4/18.
//  Copyright © 2018 Jossué Dután. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var targetLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var roundLabel: UILabel!
    @IBOutlet weak var gameSlider: UISlider!
    
    let gameModel = Game()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gameModel.restartGame()
        setValues()
        
    }
    @IBAction func playButtonPressed(_ sender: Any) {
        let sliderValue = Int(gameSlider.value)
        gameModel.play(sliderValue: sliderValue)
        
    }
    
    @IBAction func restartButtonPressed(_ sender: Any) {
        gameModel.restartGame()
    }
    
    /*private func restartGame(){
        target = Int(arc4random_uniform(100))
        scoreLabel.text = "0"
        targetLabel.text = "\(target)"
        roundLabel.text = "1"
    }*/
    
    func setValues(){
        targetLabel.text = "\(gameModel.target)"
        scoreLabel.text = "\(gameModel.score)"
        roundLabel.text = "\(gameModel.roundGame)"
    }
}

